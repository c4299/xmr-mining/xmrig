# XMRig

This project creates a Docker image that uses XMRig for mining XMR.

## Running XMRig
After installing Docker, create a shell script that pulls mhaagen/xmrig
from the Docker hub and runs it. The image requires that at least the wallet
address, the pool address and the rig id is specified. You can find an example
of a running script at:
- [run-xmrig.cmd](start-xmrig.cmd) (Windows version)
- [run-xmrig.sh](start-xmrig.sh) (Linux version)

After creating the script you just have to execute it. 

## Stopping XMRig
Stop the application by pressing CTRL+C in the console window of the executed
application. If that does not work, use the included stop scripts:
- [stop-xmrig.cmd](stop-xmrig.cmd) (Windows version)
- [stop-xmrig.sh](stop-xmrig.sh) (Linux version)
Alternatively open a second console window and type
``docker container ls``. You now get a list of all containers running on the host.
look for the one based on the image ``mhaagen/xmrig``. 
Then execute ``docker stop $CONTAINER_ID``, where $CONTAINER_ID is the ID of the container that is
displayed in the left column of ``docker container ls``. Now, after a few seconds, 
the container is shut down.