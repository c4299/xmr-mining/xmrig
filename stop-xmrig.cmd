@echo off
FOR /F "tokens=* USEBACKQ" %%F IN (`docker container ls -q -f "ancestor=mhaagen/xmrig"`) DO (SET CONTAINER_ID=%%F)
docker stop %CONTAINER_ID%
docker rm %CONTAINER_ID%
