FROM debian:stable as buildstage
RUN apt-get update && apt-get install -y curl tar

ARG VERSION=6.18.0
RUN mkdir -p "/home/download"
RUN mkdir -p "/bin/xmrig/binary"
RUN curl https://github.com/xmrig/xmrig/releases/download/v$VERSION/xmrig-$VERSION-linux-x64.tar.gz --output /home/download/xmrig -L
RUN tar -xvzf "/home/download/xmrig" --directory="/bin/xmrig/binary"
#RUN curl "https://github.com/xmrig/xmrig/blob/master/scripts/randomx_boost.sh" --output "/bin/xmrig/binary/randomx_boost.sh" -L

FROM debian:stable
RUN apt-get update && apt-get upgrade -y
RUN apt-get install msr-tools
RUN apt-get clean

ARG VERSION=6.18.0
RUN mkdir -p "/bin/xmrig/binary" && mkdir -p "/bin/xmrig/commands"
COPY --from=buildstage /bin/xmrig/binary/xmrig-$VERSION/xmrig /usr/local/bin/
COPY --from=buildstage /bin/xmrig/binary/xmrig-$VERSION/config.json /usr/local/bin/
COPY --from=buildstage /bin/xmrig/binary/xmrig-$VERSION/SHA256SUMS /usr/local/bin/
RUN chown root:root /usr/local/bin/xmrig
RUN chmod +x /usr/local/bin//xmrig
ADD binary/run-xmrig.sh /bin/xmrig/binary/
RUN chmod +x /bin/xmrig/binary/run-xmrig.sh

ENV WALLET_ADDRESS ""
ENV POOL_ADDRESS ""
ENV RIG_ID ""
ENV THREADS ""
ENTRYPOINT /bin/xmrig/binary/run-xmrig.sh
