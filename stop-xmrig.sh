#!/bin/sh
CONTAINER_ID=$(docker container ls -q -f "ancestor=mhaagen/xmrig")
echo "Test: $CONTAINER_ID"
docker stop "$CONTAINER_ID"
docker rm "$CONTAINER_ID"
