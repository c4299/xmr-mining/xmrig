#!/bin/bash
#set -x
echo "Info: Initializing..."
if [ -z "$WALLET_ADDRESS" ]
then
  echo "Error: the value of WALLET_ADDRESS is empty"
  exit 1
else
  echo "Info: WALLET_ADDRESS is $WALLET_ADDRESS"
fi

if [ -z "$POOL_ADDRESS" ]
then
  echo "Error: the value of POOL_ADDRESS is empty"
  exit 1
else
  echo "Info: POOL_ADDRESS is $POOL_ADDRESS"
fi

if [ -z "$RIG_ID" ]
then
  echo "Error: the value of RIG_ID is empty"
  exit 1
fi

OPTIONS="-o $POOL_ADDRESS -u $WALLET_ADDRESS --rig-id=$RIG_ID"

if [ -n "$THREADS" ]
then
  echo "Info: THREADS is set to $THREADS"
  OPTIONS="$OPTIONS -t $THREADS"
fi

echo "Info: Starting XMRig..."
xmrig --randomx-1gb-pages $OPTIONS
